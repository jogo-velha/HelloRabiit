package br.com.itau.HelloRabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.itau.HelloRabbit.model.Receiver;
/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App 
{
	public static String topicExchangeName = System.getenv("TOPIC");
	// = "spring-boot-exchange";
	public static String queueName = System.getenv("QUEUE") ;
	//= "spring-boot";
	public static String routeName = System.getenv("ROUTE");
	//= "foo.bar.";
	public static String fileNome = System.getenv("LOGFILE");
    @Bean
    Queue queue() {
        return new Queue(getQueueName(), false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(getTopicExchangeName());
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(getRouteName());
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
            MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(getQueueName());
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Receiver receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }
	
	
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    	System.out.println("Topic: " + getTopicExchangeName());
    	System.out.println("Queue: " + getQueueName());
    	System.out.println("route: " + getRouteName());
    	System.out.println("file.: " + getFileNome());
    }

	public static String getTopicExchangeName() {
		return topicExchangeName;
	}

	public static String getQueueName() {
		return queueName;
	}

	public static String getRouteName() {
		return routeName;
	}

	public static String getFileNome() {
		return fileNome;
	}
}

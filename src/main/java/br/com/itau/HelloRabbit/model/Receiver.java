package br.com.itau.HelloRabbit.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import br.com.itau.HelloRabbit.App;

@Component
public class Receiver {
	
    private CountDownLatch latch = new CountDownLatch(1);

	
    public void receiveMessage(String message) {
    	try {
    	SaveLog(message);
    	}
    	catch (IOException e) {
    		System.out.println("Erro ao gravar arquivo " + e.getMessage());
		}
        System.out.println("Received <" + message + ">");
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }
	public static void SaveLog(String message) throws IOException {

		BufferedWriter writer = new BufferedWriter(new FileWriter("/tmp/log.habbit", true));
		writer.write(message);
		writer.newLine();

		writer.close();
	}


}
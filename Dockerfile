FROM openjdk:8u171-jre-slim-stretch
VOLUME /tmp
ADD /target/*.jar app.jar
ADD wait-for-it.sh wait-for-it.sh
CMD ["sleep", " 60"]
CMD ["/./wait-for-it.sh", " rabbitmq:5672"]
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
VOLUME /tmp
ENV TOPIC spring-boot-exchange
ENV QUEUE spring-boot
ENV ROUTE foo.bar.env
